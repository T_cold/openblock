# openblock
官方仓库
[gitee.com/openblock](http://gitee.com/openblock)
在线编辑器地址：
https://openblock.gitee.io/

[多宿主语言、跨平台平部署的开源图形化脚本语言-OpenBlock](https://mp.weixin.qq.com/s/tzFbHB9-ze6RihwnuuLLpg)

[开源项目OpenBlock：0基础教你复刻一款经典游戏](http://t.elecfans.com/v/23162.html)


数据可视化DEMO:
[life-expectancy](https://openblock.gitee.io/frontpage/index.html#{%22proj%22:%22https://openblock.gitee.io/testing/canvas/life-expectancy/project.json%22})

模仿微信跳一跳（半成品）DEMO:
[跳一跳](https://openblock.gitee.io/frontpage/index.html#%7B%22proj%22:%22https://openblock.gitee.io/testing/canvas/jump.json%22%7D)

[更多样例](http://openblock.gitee.io/testing/)

文档和手册：

[https://openblock.gitee.io/docs/](https://openblock.gitee.io/docs/)

#### 介绍
相关视频、PPT和文档 [https://gitee.com/openblock/docs/tree/master/files](https://gitee.com/openblock/docs/tree/master/files)
openblock 主工程
包含编辑器、编译和链接
js 运行时
![界面展示](https://openblock.gitee.io/docs/manual/img/功能展示.png 'https://openblock.gitee.io/docs/manual/img/功能展示.png')
![界面展示](https://openblock.gitee.io/docs/manual/img/功能展示1.png 'https://openblock.gitee.io/docs/manual/img/功能展示1.png')

#### 软件架构

软件架构说明
![流程图](https://openblock.gitee.io/docs/manual/img/流程图.png 'https://openblock.gitee.io/docs/manual/img/流程图.png')
![流程图](https://openblock.gitee.io/docs/manual/img/流程图1.png 'https://openblock.gitee.io/docs/manual/img/流程图.png')
[GOTC-OpenBlock.pdf](//openblock.gitee.io/docs/files/GOTC-OpenBlock.pdf)
[OpenBlock_BP.pdf](//openblock.gitee.io/docs/files/OpenBlock_BP.pdf)

#### 安装教程
初学者不需要安装。
安装是参与共建或者做二次开发才需要的步骤。

首先fork主仓库到文件夹，然后执行下面的命令。
1.  npm install -g static-server
2.  static-server
3.  http://localhost:9080/frontpage/

如何共吸纳
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 如何加入

请发送申请邮件至 duzc2@163.com

### 联系

邮箱:duzc2@163.com
微信 duzc2dtw
