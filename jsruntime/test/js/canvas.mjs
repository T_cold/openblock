/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
"use strict"

class Vector2 {
    // x; y;
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class OBCanvas2D {
    // /**
    //  * @type {HTMLCanvasElement}
    //  */
    // canvas;
    // /**
    //  * @type {CanvasRenderingContext2D}
    //  */
    // canvas2dctx;
    // /**
    //  * 
    //  * @param {HTMLCanvasElement} canvas 
    //  */
    constructor(canvas) {
        this.canvas = canvas;
        this.canvas2dctx = canvas.getContext('2d');
    }
    /**
     *
     * @param {Number} color
     */
    setFillStyleColor(color) {
        let str_color;
        if (color < 0) {
            str_color = (Number.MAX_SAFE_INTEGER + color + 1).toString(16).substr(-8)
            str_color = '#' + str_color;
        } else {
            str_color = '#' + color.toString(16).padStart(8, '0');
        }
        this.canvas2dctx.fillStyle = str_color;
    }
    /**
     *
     * @param {Number} color
     */
    setStrokeStyleColor(color) {
        let str_color;
        if (color < 0) {
            str_color = (Number.MAX_SAFE_INTEGER + color + 1).toString(16).substr(-8)
            str_color = '#' + str_color;
        } else {
            str_color = '#' + color.toString(16).padStart(8, '0');
        }
        this.canvas2dctx.strokeStyle = str_color;
    }
    getVector2X(v) {
        return v.x;
    }
    getVector2Y(v) {
        return v.y;
    }
    drawImage8arg(imageName, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight, st) {
        if(!imageName){
            return;
        }
        let vm = st.fsm.VM;
        let asset = vm.assets[imageName];
        if (!asset) {
            console.warn('no asset named ' + imageName);
            return;
        }
        let rx = dWidth < 0;
        if (rx) {
            dx = -dx;
        }
        let ry = dHeight < 0;
        if (ry) {
            dy = -dy;
        }
        if (rx || ry) {
            this.canvas2dctx.save();
            this.canvas2dctx.scale(rx ? -1 : 1, ry ? -1 : 1);
        }
        this.canvas2dctx.drawImage(asset, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
        if (rx || ry) {
            this.canvas2dctx.restore();
        }
    }
    drawImage4arg(imageName, dx, dy, dWidth, dHeight, st) {
        if(!imageName){
            return;
        }
        let vm = st.fsm.VM;
        let asset = vm.assets[imageName];
        if (!asset) {
            console.warn('no asset named ' + imageName);
            return;
        }
        this.drawImage8arg(imageName, 0, 0, asset.width, asset.height, dx, dy, dWidth, dHeight, st);
    }
    drawImage2arg(imageName, dx, dy, st) {
        if(!imageName){
            return;
        }
        let vm = st.fsm.VM;
        let asset = vm.assets[imageName];
        if (!asset) {
            console.warn('no asset named ' + imageName);
            return;
        }

        this.drawImage4arg(imageName, dx, dy, asset.width, asset.height, st);
    }
    imageWidth(imageName, st) {
        let vm = st.fsm.VM;
        let asset = vm.assets[imageName];
        if (!asset) {
            console.warn('no asset named ' + imageName);
            return 0;
        }
        return asset.width;
    }
    imageHeight(imageName, st) {
        let vm = st.fsm.VM;
        let asset = vm.assets[imageName];
        if (!asset) {
            console.warn('no asset named ' + imageName);
            return 0;
        }
        return asset.height;
    }
    /**
     * 安装到脚本库
     * @param {OBScript} script 
     */
    install(script) {
        script.InstallLib("canvas2d", "canvas2d", [
            script.NativeUtil.closureVoid(this.canvas2dctx.fillRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.clearRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            // canvas2dctx.fillStyle=color
            script.NativeUtil.closureVoid(this.setFillStyleColor.bind(this), ['LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.strokeRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.fillText.bind(this.canvas2dctx), ['StringRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.beginPath.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.arc.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.fill.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.closePath.bind(this.canvas2dctx), []),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'font', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'font', 'StringRegister'),
            script.NativeUtil.closureVoid(this.canvas2dctx.arcTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.bezierCurveTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.moveTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.lineTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'textAlign', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'textAlign', 'StringRegister'),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'textBaseline', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'textBaseline', 'StringRegister'),
            script.NativeUtil.closureVoid(this.canvas2dctx.ellipse.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.rect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.rotate.bind(this.canvas2dctx), ['DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.scale.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.setStrokeStyleColor.bind(this), ['LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.stroke.bind(this.canvas2dctx), []),
            script.NativeUtil.objFieldGetter('x', 'DoubleRegister'),
            script.NativeUtil.objFieldGetter('y', 'DoubleRegister'),
            script.NativeUtil.fieldGetter(this.canvas, 'height', 'LongRegister'),
            script.NativeUtil.fieldGetter(this.canvas, 'width', 'LongRegister'),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'lineWidth', 'DoubleRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'lineWidth', 'DoubleRegister'),
            script.NativeUtil.closureVoid(this.drawImage8arg.bind(this), ['StringRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister'], true),
            script.NativeUtil.closureVoid(this.drawImage4arg.bind(this), ['StringRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister'], true),
            script.NativeUtil.closureVoid(this.drawImage2arg.bind(this), ['StringRegister', 'DoubleRegister', 'DoubleRegister'], true),
            script.NativeUtil.closureReturnValue(this.imageWidth.bind(this), 'DoubleRegister', ['StringRegister'], true),
            script.NativeUtil.closureReturnValue(this.imageHeight.bind(this), 'DoubleRegister', ['StringRegister'], true),
            script.NativeUtil.closureVoid(this.canvas2dctx.save.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.restore.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.setTransform.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.transform.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.translate.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.clip.bind(this.canvas2dctx), []),
        ]);
    }
}

// module.exports = {OBCanvas2D};
export { OBCanvas2D, Vector2 };