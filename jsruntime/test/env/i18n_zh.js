/**
 * @license
 * Copyright 2022 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
/*jshint -W117 */
Blockly.Msg["touchstart"] = "触摸按下";
Blockly.Msg["touchmove"] = "触摸移动";
Blockly.Msg["touchcancel"] = "触摸取消";
Blockly.Msg["touchend"] = "触摸停止";
Blockly.Msg["click"] = "点击";
Blockly.Msg["longpress"] = "长按";
Blockly.Msg["swipe"] = "快速滑动";
Blockly.Msg["windowResize"] = "窗口改变大小";


Blockly.Msg["width"] = "宽";
Blockly.Msg["height"] = "高";
Blockly.Msg["Canvas2d_fillRect"] = "绘制实心矩形";
Blockly.Msg["Canvas2d_strokeRect"] = "绘制描边矩形";
Blockly.Msg["Canvas2d_clearRect"] = "清空矩形区域";
Blockly.Msg["Canvas2d_setFillStyleColor"] = "设置填充颜色";
Blockly.Msg["Canvas2d_setStrokeStyleColor"] = "设置描边颜色";
Blockly.Msg["Canvas2d_fillText"] = "绘制实心文字";
Blockly.Msg["Canvas2d_beginPath"] = "开始路径";
Blockly.Msg["Canvas2d_arc"] = "创建弧形";
Blockly.Msg["Canvas2d_fill"] = "填充路径";
Blockly.Msg["Canvas2d_stroke"] = "描边路径";
Blockly.Msg["Canvas2d_closePath"] = "关闭路径";
Blockly.Msg["Canvas2d_setFont"] = "设置字体";
Blockly.Msg["Canvas2d_getFont"] = "当前字体";
Blockly.Msg["Canvas2d_arcTo"] = "依据圆弧经过的点和圆弧半径创建圆弧路径";
Blockly.Msg["Canvas2d_bezierCurveTo"] = "创建三次贝赛尔曲线路径到";
Blockly.Msg["Canvas2d_moveTo"] = "移动到指定点";
Blockly.Msg["Canvas2d_lineTo"] = "到指定点进行路径连接";
Blockly.Msg["Canvas2d_setTextAlign"] = "文本绘制中的文本对齐方式";
Blockly.Msg["Canvas2d_getTextAlign"] = "文本绘制中的文本对齐方式";
Blockly.Msg["Canvas2d_setTextAlign_options"] = [
    "left", "right", "center", "start", "end"
];
Blockly.Msg["left"] = "左";
Blockly.Msg["right"] = "右";
Blockly.Msg["center"] = "中";
Blockly.Msg["start"] = "开始";
Blockly.Msg["end"] = "结束";
Blockly.Msg["Canvas2d_setTextBaseline"] = "文字垂直方向的对齐方式";
Blockly.Msg["Canvas2d_getTextBaseline"] = "文字垂直方向的对齐方式";
Blockly.Msg["Canvas2d_setTextBaseline_options"] = [
    "alphabetic", "top", "hanging", "middle", "ideographic", "bottom"
];
Blockly.Msg["alphabetic"] = "字母基线";
Blockly.Msg["top"] = "顶部";
Blockly.Msg["hanging"] = "悬挂";
Blockly.Msg["middle"] = "中间";
Blockly.Msg["ideographic"] = "表意字基线";
Blockly.Msg["bottom"] = "底部";
Blockly.Msg["align"] = "对齐";
Blockly.Msg["Canvas2d_ellipse"] = "创建椭圆路径";
Blockly.Msg["Canvas2d_rect"] = "创建矩形路径";
Blockly.Msg["Canvas2d_rotate"] = "顺时针旋转(弧度)";
Blockly.Msg["Canvas2d_scale"] = "缩放倍数";
Blockly.Msg["keydown"] = "按下按键";
Blockly.Msg["keyup"] = "抬起按键";
Blockly.Msg["Canvas2d_height"] = "画板高度";
Blockly.Msg["Canvas2d_width"] = "画板宽度";

Blockly.Msg["Vector2"] = "二维向量";
Blockly.Msg["vector2"] = "二维向量";
Blockly.Msg["Vector2_x"] = "二维向量的x";
Blockly.Msg["Vector2_y"] = "二维向量的y";

Blockly.Msg["radius"] = "半径";
Blockly.Msg["radiusX"] = "X轴半径";
Blockly.Msg["radiusY"] = "Y轴半径";
Blockly.Msg["radian"] = "弧度";
Blockly.Msg["rotation"] = "旋转";
Blockly.Msg["startAngle"] = "起始弧度（0-2π）";
Blockly.Msg["endAngle"] = "结束弧度（0-2π）";
Blockly.Msg["anticlockwise"] = "逆时针";

Blockly.Msg["cp1x"] = "第一控制点的x";
Blockly.Msg["cp1y"] = "第一控制点的y";
Blockly.Msg["cp2x"] = "第二控制点的x";
Blockly.Msg["cp2y"] = "第二控制点的y";

Blockly.Msg["Canvas2d_setLineWidth"] = "设置描边宽度";
Blockly.Msg["Canvas2d_getLineWidth"] = "描边宽度";


Blockly.Msg["Canvas2d_drawImage"] = "绘制图像";
Blockly.Msg["Canvas2d_drawImage2"] = "绘制图像";
Blockly.Msg["Canvas2d_drawImage4"] = "绘制图像";
Blockly.Msg["imageFileName"] = "图像名称";
Blockly.Msg["SourceX"] = "原图X剪裁";
Blockly.Msg["SourceY"] = "原图Y剪裁";
Blockly.Msg["SourceWidth"] = "原图宽剪裁";
Blockly.Msg["SourceHeight"] = "原图高剪裁";
Blockly.Msg["DestX"] = "绘制X坐标";
Blockly.Msg["DestY"] = "绘制Y坐标";
Blockly.Msg["DestWidth"] = "绘制宽度";
Blockly.Msg["DestHeight"] = "绘制高度";

Blockly.Msg["Canvas2d_ImageWidth"] = "图像原始宽度";
Blockly.Msg["Canvas2d_ImageHeight"] = "图像原始高度";

Blockly.Msg["Canvas2d_Save"] = "保存画板状态";
Blockly.Msg["Canvas2d_Restore"] = "恢复画板状态";

Blockly.Msg["Canvas2d_SetTransform"] = "设置变换矩阵";
Blockly.Msg["Canvas2d_Transform"] = "矩阵变换";
Blockly.Msg["HorizontalScaling"] = "水平缩放";
Blockly.Msg["VerticalSkewing"] = "垂直倾斜";
Blockly.Msg["HorizontalSkewing"] = "水平倾斜";
Blockly.Msg["VerticalScaling"] = "垂直缩放";
Blockly.Msg["HorizontalTranslation"] = "水平移动";
Blockly.Msg["VerticalTranslation"] = "垂直移动";

Blockly.Msg["Canvas2d_Translate"] = "移动坐标系原点";

Blockly.Msg["Canvas2d_Clip"] = "当前路径设置为剪切路径";
