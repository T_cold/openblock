/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
OpenBlock.onInited(() => {
    let simulator = new Simulator();
    OpenBlock.addConnector(simulator);
    OpenBlock.Utils.handleUrlHash(() => {
        simulator.loading = true;
        showOverallLoading();
    }, () => {
        simulator.loading = false;
        hideOverallLoading();
    });
});