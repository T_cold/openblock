/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

(function () {
    let componentName = 'openblock-help';
    let Help = Object.create(null);
    OpenBlock.Help = Help;
    Help.docCache = Object.create(null);
    Help.blocklyHelpUrl = Object.create(null);
    Help.docPath = [];
    Help.enabled = true;
    Help.currentDoc = "";
    function loadHelpDoc(type) {
        // 设置为true以防止连续请求时一直加载
        Vue.set(Help.docCache, type, true);
        let pathIdx = 0;
        function r() {
            if (pathIdx >= Help.docPath.length) {
                Vue.set(Help.docCache, type, false);
                return;
            }
            let path = Help.docPath[pathIdx];
            pathIdx++;
            OpenBlock.Utils.requestText(path + type + ".md", (data) => {
                if (!data) {
                    r();
                } else {
                    Vue.set(Help.docCache, type, { path, data });
                }
            });
        }
        r();
    }
    function checkHelpDoc(type) {
        if (!Help.docCache[type]) {
            loadHelpDoc(type);
        }
    }
    Help.openHelpWindow = function (block_type) {
        checkHelpDoc(block_type)
        Help.currentDoc = block_type;
        Help.enabled = true;
        UB_IDE.ensureExtComponent('subwindows',componentName);
    };
    var reader = new commonmark.Parser();
    var writer = new commonmark.HtmlRenderer();


    $.ajax({
        url: 'js/htmls/help/htmls.html',
        dataType: 'text',
        async: false,
        success(data, status, xhr) {
            let template = data;
            Vue.component(componentName, {
                data() { return Help; },
                template: template,
                computed: {
                    marked() {
                        let d = Help.docCache[Help.currentDoc];
                        var parsed = reader.parse(d.data);
                        var walker = parsed.walker();

                        var event, node;

                        while ((event = walker.next())) {
                            node = event.node;
                            if (node.type === 'image') {
                                if (node.destination.startsWith('.')) {
                                    node.destination = d.path + node.destination.substring(1);
                                }
                            }
                        }
                        var result = writer.render(parsed);
                        return result;
                    }
                }
            });
        }
    });
    OpenBlock.onInited(() => {
        let origNewBlock = Blockly.WorkspaceSvg.prototype.newBlock;
        Blockly.WorkspaceSvg.prototype.newBlock = function (prototypeName, opt_id) {
            let nb = origNewBlock.call(this, prototypeName, opt_id);
            if (OpenBlock.BlocklyParser.BlockToAST[prototypeName]) {
                if (nb.helpUrl && !Help.blocklyHelpUrl[prototypeName]) {
                    let url = nb.helpUrl;
                    Vue.set(Help.blocklyHelpUrl, prototypeName, url);
                }
                nb.setHelpUrl();
            }
            return nb;
        };
        Help.docPath.push(OpenBlock.bootPath + "docs/../../../block-docs/" + OpenBlock.language + "/");
        Help.docPath.push(OpenBlock.bootPath + "docs/../../../block-docs/");
        Blockly.ContextMenuRegistry.registry.register({
            id: "openblock-help",
            scopeType: Blockly.ContextMenuRegistry.ScopeType.BLOCK,
            displayText: Blockly.Msg.HELP,
            preconditionFn() {
                return 'enabled';
            },
            weight: 15,
            callback(blocksvg) {
                let type = blocksvg.block.type;
                if (type === 'native_call') {
                    type = blocksvg.block.mutationData.func.fullname;
                }
                Help.openHelpWindow(type);
            }
        });

        Blockly.ContextMenuRegistry.registry.register({
            id: "openblock-showcode",
            scopeType: Blockly.ContextMenuRegistry.ScopeType.WORKSPACE,
            displayText: 'XML',
            preconditionFn() {
                return 'enabled';
            },
            weight: 100,
            callback(e) {
                let dom = Blockly.Xml.workspaceToDom(e.workspace);
                let txt = Blockly.Xml.domToText(dom);
                console.log(txt);
            }
        });
    });
})();