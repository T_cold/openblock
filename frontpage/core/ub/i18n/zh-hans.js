OpenBlock.I18N.START_SRC_NAME = 'Start';
OpenBlock.I18N.NEW_SRC_NAME = '新建模块';
OpenBlock.I18N.START_FSM_NAME = 'Main';
OpenBlock.I18N.NEW_FSM_TYPE_NAME = '新状态机类型';
OpenBlock.I18N.NEW_STATE_NAME = '状态';
OpenBlock.I18N.NEW_STRUCT_NAME = '数据结构';
OpenBlock.I18N.NEW_FUNCTION_NAME = '函数';
OpenBlock.I18N.Number = '数字';
OpenBlock.I18N.Integer = '整数';
OpenBlock.I18N.Boolean = '布尔';
OpenBlock.I18N.String = '字符串';
OpenBlock.I18N.FSM = '状态机';
OpenBlock.I18N.COLOR = '颜色';
Blockly.Msg["Number"] = "数字";
Blockly.Msg["String"] = "字符串";
Blockly.Msg["Boolean"] = "布尔";
Blockly.Msg["Integer"] = "整数";
Blockly.Msg["FSM"] = "状态机";
OpenBlock.I18N.PRIMARY_TYPES = [
    [OpenBlock.I18N.Number, 'Number'],
    [OpenBlock.I18N.Integer, 'Integer'],
    [OpenBlock.I18N.Boolean, 'Boolean'],
    [OpenBlock.I18N.String, 'String'],
    [OpenBlock.I18N.FSM, 'FSM'],
    [OpenBlock.I18N.COLOR, 'Colour']
];
Blockly.Msg["Colour"] = OpenBlock.I18N.COLOR;
Blockly.Msg["colour"] = OpenBlock.I18N.COLOR;
Blockly.Msg["text"] = OpenBlock.I18N.String;
Blockly.Msg["destroy_fsm"] = "销毁状态机";
Blockly.Msg["variables_self"] = "当前状态机";
Blockly.Msg["logic_is_not_valid"] = "%1 为无效值";
Blockly.Msg["logic_is_valid"] = "%1 为有效值";
Blockly.Msg["controls_yield"] = "等待一帧";
Blockly.Msg["change_state"] = "改变状态为 %1";
Blockly.Msg["on_message"] = "当收到消息 %1";
Blockly.Msg["on_message_struct"] = "当收到消息 %1 附加结构体数据 %2";
Blockly.Msg["on_message_primary"] = "当收到消息 %1 附加基本类型数据 %2";
Blockly.Msg["fsm_variables_get"] = "状态机变量 %2 %1";
Blockly.Msg["fsm_variables_set"] = "设置状态机变量 %3 %1 为 %2";
Blockly.Msg["state_variables_get"] = "状态变量 %2 %1";
Blockly.Msg["state_variables_set"] = "设置状态变量 %3 %1 为 %2";
Blockly.Msg["fsm_create"] = "创建 %1 类型的状态机";
Blockly.Msg["fsm_create_unnamed"] = "未命名";
Blockly.Msg["find_fsm_by_type"] = "搜索类型为 %1 的状态机";
Blockly.Msg["find_fsm_by_name"] = "搜索名为 %1 的状态机";

Blockly.Msg["Network_join"] = "加入网络";
Blockly.Msg["Network_is_joined"] = "已加入网络";
Blockly.Msg["Network_leave"] = "断开网络";
Blockly.Msg["Network_send_message"] = "发送远程消息";
Blockly.Msg["Network_target"] = "远程目标";
Blockly.Msg["Network_on_received"] = "当收到远程消息";
Blockly.Msg["network_peer_join"] = "当远程目标可用";
Blockly.Msg["network_peer_leave"] = "当远程目标离线";
Blockly.Msg["Network_is_network_message"] = "当前消息是否来自于远程";
Blockly.Msg["Network_enable"] = "接收网络消息";
Blockly.Msg["Network_set_enable"] = "设置当前状态机接收网络消息";
Blockly.Msg["Network_is_enabled"] = "当前状态机是否接受网络消息";


Blockly.Msg["target"] = "目标";
Blockly.Msg["data"] = "数据";
Blockly.Msg["any"] = "任意";
Blockly.Msg["enabled"] = "启用";

Blockly.Msg['Simulator'] = '模拟器';