/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
 (function () {

    OpenBlock.Blocks.toolbox_search_flyout = function (workspace) {
        var xmlList = [];
        var b = Blockly.utils.xml.createElement('input');
        b.setAttribute('type', 'text');
        var m = Blockly.utils.xml.createElement('mutation');
        m.setAttribute('eventname', "Start");
        m.setAttribute('style', "event_blocks");
        b.appendChild(m);
        xmlList.push(b);
        return xmlList;
    };
    OpenBlock.Blocks.build_toolbox_search_flyout = function (workspace) {
        workspace.registerToolboxCategoryCallback('TOOLBOX_SEARCH_CATEGROY', OpenBlock.Blocks.toolbox_search_flyout);
    };
    OpenBlock.wsBuildCbs.push(OpenBlock.Blocks.build_toolbox_search_flyout);
})();