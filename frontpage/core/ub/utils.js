/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
(function () {
    let imgsuffixs = ['.png', '.jpg', '.jpeg'];
    let soundsuffixs = ['.wav', '.mp3'];
    let videosuffixs = ['.mp4'];
    OpenBlock.Utils = {
        requestText(url, callback, async) {
            $.ajax({
                type: 'GET',
                url,
                dataType: 'text',
                async: typeof (async) == 'undefined' ? true : async,
                success: function (data) {
                    callback(data);
                },
                error(xhr, status, err) {
                    callback(null, err, xhr, status);
                }
            });
        },
        requestBin(url, callback, async) {
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, typeof (async) == 'undefined' ? true : async);
            oReq.responseType = "arraybuffer";

            oReq.onload = function (oEvent) {
                var arrayBuffer = oReq.response;
                callback(arrayBuffer);
            };

            oReq.send();
        },
        invalidChar() {
            return "`·￥！?~!@#$%^&*()+=`\\/\"'.><,，。;:|\n\r\t？【】{}[]（）";
        },
        isIdentifier(str) {
            if (str == null || str.length == 0) {
                return false;
            }
            let c = OpenBlock.Utils.invalidChar();
            for (let i = 0; i < c.length; i++) {
                if (str.indexOf(c[i]) >= 0) {
                    return false;
                }
            }
            return true;
        },
        makeSN() {
            let r = Math.random() * Number.MAX_SAFE_INTEGER;
            let sn = Date.now().toString(36) + "-" + Math.floor(r).toString(36);
            return sn;
        },
        hasName(arr, name) {
            for (let a of arr) {
                if (a.name === name) {
                    return true;
                }
            }
            return false;
        },

        genName(base, checkArr) {
            let postfix = 1, newname = base;
            while (OpenBlock.Utils.hasName(checkArr, newname)) {
                newname = `${base}_${postfix++}`;
            }
            return newname;
        },
        loadProjectFile(projpath, callback) {

            let projRoot = projpath.substring(0, projpath.lastIndexOf('/') + 1);
            OpenBlock.Utils.requestText(projpath, (data, xhr, status, err) => {
                if (err) {
                    UB_IDE.$Notice.error({
                        title: '工程打开失败！'
                    });
                    callback();
                    return;
                }
                if (!data) {
                    UB_IDE.$Notice.error({
                        title: '工程打开失败！'
                    });
                    callback();
                    return;
                }
                data = JSON.parse(data);
                let loading = 0;
                let loadedSrc = [];
                function checkLoaded() {
                    if (loading === 0) {
                        OpenBlock.DataImporter.reorganizeData();
                        OpenBlock.exportExePackage(() => {
                            UB_IDE.$Notice.success({
                                title: OpenBlock.i('工程加载完成')
                            });
                        });
                        try {
                            VFS.partition.src.putAll(loadedSrc);
                        } catch (e) {
                            console.warn(e);
                        }
                        callback();
                    }
                }
                for (key1 in data) {
                    let key = key1;
                    let list = data[key];
                    list.forEach(
                        /**
                         * 
                         * @param {String} item 
                         */
                        item => {
                            let i1 = item.lastIndexOf('/');
                            let name;
                            if (i1 >= 0) {
                                name = item.substring(i1 + 1);
                            } else {
                                name = item;
                            }
                            if (item.indexOf('://') === -1) {
                                item = projRoot + item;
                            }
                            if (key === 'src') {
                                loading++;
                                OpenBlock.Utils.requestText(item, (data, err) => {
                                    if (data) {
                                        let src = JSON.parse(data);
                                        loadedSrc.push({ name: src.name + '.xs', content: src });
                                    } else {
                                        console.warn(err);
                                    }
                                    loading--;
                                    checkLoaded();
                                });
                            } else {
                                let vfs = VFS.partition[key];
                                if (vfs) {
                                    loading++;
                                    OpenBlock.Utils.requestBin(item, data => {
                                        loading--;
                                        vfs.put(name, data);
                                        checkLoaded();
                                    });
                                }
                            }
                        });
                }
                if (loading === 0) {
                    callback();
                }
            });
        },
        /**
         * 处理hash，认为hash是json内容
         * @param {function} onloading 
         * @param {function} onloaded 
         */
        handleUrlHash(onloading, onloaded) {
            if (window.location.hash) {
                let hash = window.location.hash;
                if (hash.length > 0) {
                    hash = hash.substring(1);
                    hash = decodeURIComponent(hash);
                    let hashobj = JSON.parse(hash);
                    // 处理 proj参数
                    if (hashobj.proj) {
                        onloading();
                        UB_IDE.$Notice.open({
                            title: '正在打开工程，请稍后。'
                        });
                        OpenBlock.Utils.loadProjectFile(hashobj.proj, onloaded);
                    }
                }
            }
        },
        /**
         *   let url= arrayBufferToBase64(response);
         *   document.getElementById('img').src='data:image/jpeg;base64,'+url;
         * @param {ArrayBuffer} buffer 
         * @returns 
         */
        arrayBufferToBase64(buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        },
        /**
         * 
         * @param {String} name 
         * @param {String[]} suffixs 
         * @returns 
         */
        endsWith(name, suffixs) {
            for (i = 0; i < suffixs.length; i++) {
                if (name.endsWith(suffixs[i])) {
                    return true;
                }
            }
            return false;
        },
        soundsuffixs, imgsuffixs, videosuffixs,
        /**
         * 
         * @param {String} name 必须小写
         * @returns 
         */
        mediaType(name) {
            /**
             * @type String
             */
            if (OpenBlock.Utils.endsWith(name, soundsuffixs)) {
                return 'sound';
            }
            if (OpenBlock.Utils.endsWith(name, imgsuffixs)) {
                return 'image';
            }
            if (OpenBlock.Utils.endsWith(name, videosuffixs)) {
                return 'video';
            }
            console.warn('Unknown mediaType:' + filename);
            return 'Unknown';
        },
        /**
         * 
         * @param {String} filename 必须小写
         * @returns 
         */
        fileType(filename) {
            let idx = filename.lastIndexOf('.');
            if (idx >= 0) {
                return filename.substring(idx);
            } else {
                return "";
            }
        },
        eventsSkippedSaving: [
            Blockly.Events.UI,
            Blockly.Events.BLOCK_DRAG,
            Blockly.Events.SELECTED,
            Blockly.Events.CLICK,
            Blockly.Events.BUBBLE_OPEN,
            Blockly.Events.TRASHCAN_OPEN,
            Blockly.Events.TOOLBOX_ITEM_SELECT,
            Blockly.Events.THEME_CHANGE,
            Blockly.Events.VIEWPORT_CHANGE,
        ],

    };
})();